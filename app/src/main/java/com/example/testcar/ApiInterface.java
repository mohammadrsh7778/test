package com.example.testcar;

import com.example.testcar.models.Post;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {
    public static final String BASE_URL = "http://172.20.10.2:9092/";

    @POST("users")
    Call<Post> loginUser(@Body Post post);
}

package com.example.testcar;

import android.util.Log;

import com.example.testcar.models.Post;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    public void start(Post post) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiInterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
       Call<Post> call = apiInterface.loginUser(post);
       call.enqueue(new Callback<Post>() {
           @Override
           public void onResponse(Call<Post> call, Response<Post> response) {
               if (response.isSuccessful()) {
                   Log.i("TESTSERVICE", "yesssssss");

               } else {
                   Log.i("TESTSERVICE", "noooooo");

               }
               Log.i("TESTSERVICE", "ok" + "--" + response.body());
           }

           @Override
           public void onFailure(Call<Post> call, Throwable t) {
               Log.e("TESTSERVICE", "Unable Unable to submit  post to API."+ t.getCause());

           }
       });
    }

}

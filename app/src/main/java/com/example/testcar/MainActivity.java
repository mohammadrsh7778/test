package com.example.testcar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.testcar.models.Post;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    EditText edtPhoneNumber;
    Button btn;
    Post post;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();

    }


    private void findViews() {
        edtPhoneNumber = findViewById(R.id.edtPhoneNumber);
        btn = findViewById(R.id.btn);

        edtPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                post = new Post();
                if (s.length() == 11) {
                    post.setPhoneNumber(s.toString());
                    RetrofitClient retrofitClient=new RetrofitClient();
                    retrofitClient.start(post);



                }


            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
  //    btn.setOnClickListener(new View.OnClickListener() {
  //        @Override
  //        public void onClick(View v) {
  //            Log.i("TESTSERVICE", "jnlknl;d" + post.getPhoneNumber());


  //        }
  //    });


    }


}
